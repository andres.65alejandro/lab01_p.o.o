/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01;

import java.util.Date;
import java.util.Scanner;

/**
 *
 * @Andres lopez 2019160378
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Cuenta cuenta = new Cuenta(1122,500000);
        cuenta.setTasaDeInteresAnual(0.045);
        cuenta.retirarDinero(200000);
        
        double interesMensual = cuenta.calcularInteresMensual();
        Date fecha = cuenta.getFechaDeCreacion();
        
        System.out.println("Balance actual " + cuenta.getBalance());
        System.out.println("Interes mensual " + interesMensual);
        System.out.println("Fecha de creacion " + fecha);
        
        
        
        Cuenta cuenta2 = new Cuenta(1314, 100000);
        double interesMensual2 = cuenta2.calcularInteresMensual();
        Date fecha2 = cuenta2.getFechaDeCreacion();
        
        System.out.println("Balance actual " + cuenta2.getBalance());
        System.out.println("Interes mensual " + interesMensual2);
        System.out.println("Fecha de creacion " + fecha2);
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        
        
        ATM atm = new ATM();  
    }
}
    

