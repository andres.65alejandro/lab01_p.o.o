/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01;

import java.util.Scanner;

/**
 *
 * @Andres lopez 2019160378
 */
public class ATM {
    private Cuenta[] cuentas;
    private int idActual;

    public ATM() {
        this.cuentas = new Cuenta[10];
        crearDiezCuentas();
        solicitarID();
        mostrarATM();
    }
    
    private void crearDiezCuentas(){
        
        for (int i = 0; i < 10; i++) {
            this.cuentas[i] = new Cuenta(i,100000);
            this.cuentas[i].setTasaDeInteresAnual(0.045);
            
        }
    }
    
    public int solicitarID(){
        boolean usuarioValido = false;
        int id = -1;
        
        while(usuarioValido == false){
            System.out.println("Ingrese su ID: ");
            Scanner entrada = new Scanner(System.in);
            String idString = entrada.nextLine();
            id = Integer.parseInt(idString);
        
            usuarioValido = existeId(id);
        
        }
        this.idActual = id;        
        return id;
    }
    
    private boolean existeId(int id){
        boolean existe = false;
        for (int i = 0; i < cuentas.length; i++) {
            if(cuentas[i].getId() == id){
                existe = true;
            }
        }
        return existe;
    }
    
    public void RetirarDinero(int dinero, int idUsuario){
        Cuenta usuarioActual = getUsuarioActual(idUsuario);
        usuarioActual.retirarDinero(dinero);
    }
    public void DepositarDinero(int dinero, int idUsuario){
        Cuenta usuarioActual = getUsuarioActual(idUsuario);
        usuarioActual.depositarDinero(dinero);
    }
    public void verBalanceActual(int idUsuario){
        Cuenta usuarioActual = getUsuarioActual(idUsuario);
        System.out.println("El balance actual es: " + usuarioActual.getBalance());
    }
    
    private Cuenta getUsuarioActual(int idUsuario){
        Cuenta usuarioActual = null;
        for (int i = 0; i < cuentas.length; i++) {
            if(cuentas[i].getId() == idUsuario){
                usuarioActual = cuentas[i];
            }
        }
        return usuarioActual;
    }
    
    public void mostrarATM(){
        
        
        System.out.println("1. Ver balance actual");
        System.out.println("2. Retirar dinero");
        System.out.println("3. Depositar dinero");
        System.out.println("4. Salir");
        
        Scanner entrada = new Scanner(System.in);
        String idString = entrada.nextLine();
        int decision = Integer.parseInt(idString);
        
        if (decision == 1){
            verBalanceActual(idActual);
            mostrarATM();
        }
        else if(decision == 2){
            System.out.println("Escriba la cantidad que desea retirar");
            Scanner entrada2 = new Scanner(System.in);
            String montostring = entrada.nextLine();
            int monto = Integer.parseInt(montostring);
            
            RetirarDinero(monto, idActual);
            System.out.println("Dinero retirado");
            mostrarATM();
        }
        else if(decision == 3){
            System.out.println("Escriba la cantidad que desea depositar");
            Scanner entrada3 = new Scanner(System.in);
            String montostring = entrada.nextLine();
            int monto = Integer.parseInt(montostring);
            
            DepositarDinero(monto, idActual);
            System.out.println("Dinero depositado");
            mostrarATM();
        }
        else if(decision == 4){
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            solicitarID();
        }
        
    }
}
