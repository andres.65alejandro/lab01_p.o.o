package lab01;

import java.util.Date;

/**
 *
 * @Andres lopez 2019160378
 */
public class Cuenta {
    private int id = 0;
    private double balance = 0;
    private double tasaDeInteresAnual = 0;
    private Date fechaDeCreacion;
    
    public Cuenta() {
        fechaDeCreacion = new Date();
    }

    public Cuenta(int id, double balance) {
        this.id = id;
        this.balance = balance;
        fechaDeCreacion = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getTasaDeInteresAnual() {
        return tasaDeInteresAnual;
    }

    public void setTasaDeInteresAnual(double tasaDeInteresAnual) {
        this.tasaDeInteresAnual = tasaDeInteresAnual;
    }

    public Date getFechaDeCreacion() {
        return fechaDeCreacion;
    }
    
    public double obtenerTasaDeInteresMensual(){
        double interesMensual = tasaDeInteresAnual % 12;
        return interesMensual;
    }
    
    public double calcularInteresMensual(){
        
        double interesMensual = balance * tasaDeInteresAnual;
        return interesMensual;
    }
    
    public void retirarDinero(int monto){
        this.balance -= monto;
    } 
    
    public void depositarDinero(int monto){
        this.balance += monto;
    } 
    
}
